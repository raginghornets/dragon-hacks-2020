import React, { Component } from 'react';
import {
  Person,
  lookupProfile
} from 'blockstack';

const avatarFallbackImage = 'https://s3.amazonaws.com/onename/avatar-placeholder.png';
const STATUSES_FILENAME = 'statuses.json';
const GALLERY_FILENAME = 'gallery.json';

export default class Profile extends Component {
  constructor(props) {
  	super(props);

  	this.state = {
  	  person: {
  	  	name() {
          return 'Anonymous';
        },
  	  	avatarUrl() {
  	  	  return avatarFallbackImage;
        },
        disliked_ids: [],
        liked_ids: [],
        username: "",
        newStatus: "",
        statuses: [],
        statusIndex: 0,
        isLoading: false,
        newPicture: "",
        gallery: [],
        galleryIndex: 0,
        isLoadingGallery: false
  	  },
  	};
  }

  renderNavBar() {
    const { handleSignOut } = this.props;
    const { person } = this.state;

    return (
      <nav className="navbar navbar-expand-md navbar-dark bg-blue fixed-top">
        <a href={ person.avatarUrl() }>
          <img src={person.avatarUrl() ? person.avatarUrl() : './avatar-placeholder.png'} class="avatar" width="25" height="25"alt=""/>
        </a>
        <p id="personName">{ person.name() }</p>
        <a href="/" className="title"><h1>Date Block</h1></a>
        {this.isLocal() &&
          <button
            className="btn btn-primary btn-sm"
            id="signout-button"
            onClick={ handleSignOut.bind(this) }
          >Logout</button>
        }
      </nav>
    );
  }

  renderGallery() {
    return (
      <div className="col-md-12 gallery">
        {this.state.isLoadingGallery && <span>Loading...</span>}
        {this.state.gallery && this.state.gallery.map((picture) => (
            <div className="picture" key={picture.id}>
              <div class="timestamp">
                {new Date(picture.created_at).toLocaleDateString("en-US",
                  { weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric' })}
              </div>
              <img src={picture.url} alt="Gallery entry" className="gallery-entry"/>
            </div>
          )
        )}
      </div>
    );
  }

  renderNewPicture() {
    return (
      <div className="new-picture">
        <div className="col-md-12"><input type="url" id="new-gallery-picture" placeholder="Enter an image URL"/>
        </div>
        <div className="col-md-12">
          <button
            className="btn btn-primary btn-lg"
            onClick={e => this.handleNewPictureSubmit(e)}
          >
            Submit
          </button>
        </div>
      </div>
    );
  }

  renderStatuses() {
    return (
      <div className="col-md-12 statuses">
        {this.state.isLoading && <span>Loading...</span>}
        {this.state.statuses && this.state.statuses.map((status) => (
            <div className="status" key={status.id}>
              <div class="timestamp">
                {new Date(status.created_at).toLocaleDateString("en-US",
                  { weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric' })}
              </div>
              {status.text}
            </div>
          )
        )}
      </div>
    );
  }

  renderNewStatus() {
    return (
      <div className="new-status">
        <div className="col-md-12">
          <textarea className="input-status"
            value={this.state.newStatus}
            onChange={e => this.handleNewStatusChange(e)}
            placeholder="Enter a status"
          />
        </div>
        <div className="col-md-12">
          <button
            className="btn btn-primary btn-lg"
            onClick={e => this.handleNewStatusSubmit(e)}
          >
            Submit
          </button>
        </div>
      </div>
    );
  }

  renderPersonalInfo() {
    const { person } = this.state;

    return (
      <div className="personal-info">
        <img src={person.avatarUrl() ? person.avatarUrl() : './avatar-placeholder.png'} class="avatar-main" alt=""/>
        <p className="name">{ person.name() }</p>
        <p className="description">{ person.description() }</p>
      </div>
    );
  }

  renderLikeButtons() {
    return (
      <div className="like-buttons">
        <button className="btn btn-primary" onClick={(e) => {
          this.handleNewStatusChange(e);
          this.saveNewStatus('I disliked someone.')}}>DISLIKE</button>
        <button className="btn btn-primary" onClick={(e) => {
          this.handleNewStatusChange(e);
          this.saveNewStatus('I liked someone!')}}>LIKE</button>
      </div>
    );
  }

  render() {
    const { userSession } = this.props;
    const { person } = this.state;
    document.title = person.name();

    return (
        !userSession.isSignInPending() ?
      <div className="panel-welcome row" id="section-2">
      { this.renderNavBar() }
        <div className="col-md-4">
          <h5>Statuses</h5>
          { this.renderStatuses() }
          { this.isLocal() && this.renderNewStatus() }
        </div>
        <div className="col-md-4 ">
          <h5>Gallery</h5>
          { this.renderGallery() }
          { this.isLocal() && this.renderNewPicture() }
        </div>
        <div className="col-md-4 right-side">
          { this.renderPersonalInfo() }
          { !this.isLocal() && this.renderLikeButtons() }
        </div>
      </div> : null
    );
  }

  componentWillMount() {
    const { userSession } = this.props;
    this.setState({
      person: new Person(userSession.loadUserData().profile),
      username: userSession.loadUserData().username
    });
  }

  componentDidMount() {
    this.fetchData();
  }

  handleNewStatusChange(event) {
    this.setState({newStatus: event.target.value});
  }

  handleNewStatusSubmit(event) {
    this.saveNewStatus(this.state.newStatus);
    this.setState({
      newStatus: ""
    });
  }

  handleNewPictureSubmit(event) {
    this.saveNewPicture(document.getElementById('new-gallery-picture').value);
  }
  
  saveNewStatus(statusText) {
    const { userSession } = this.props;
    let statuses = this.state.statuses;
    this.setState({statusIndex: (this.state.statusIndex + 1)});

    let status = {
      id: this.state.statusIndex,
      text: statusText.trim(),
      created_at: Date.now()
    };

    statuses.unshift(status);
    const options = { encrypt: false };
    userSession.putFile(STATUSES_FILENAME, JSON.stringify(statuses), options)
      .then(() => {
        this.setState({
          statuses: statuses
        })
      });
  }

  saveNewPicture(pictureUrl) {
    const { userSession } = this.props;
    let gallery = this.state.gallery;
    this.setState({galleryIndex: (this.state.galleryIndex + 1)});

    let picture = {
      id: this.state.galleryIndex,
      url: pictureUrl.trim(),
      created_at: Date.now()
    };

    gallery.unshift(picture);
    const options = { encrypt: false };
    userSession.putFile(GALLERY_FILENAME, JSON.stringify(gallery), options)
      .then(() => {
        this.setState({
          gallery: gallery
        })
      });
  }

  fetchData() {
    const { userSession } = this.props;
    this.setState({ isLoading: true, isLoadingGallery: true });
    if (this.isLocal()) {
      const options = { decrypt: false };
      userSession.getFile(STATUSES_FILENAME, options)
        .then((file) => {
          var statuses = JSON.parse(file || '[]');
          this.setState({
            person: new Person(userSession.loadUserData().profile),
            username: userSession.loadUserData().username,
            statusIndex: statuses.length,
            statuses: statuses,
          })
        })
        .finally(() => {
          this.setState({ isLoading: false })
        });
      userSession.getFile(GALLERY_FILENAME, options)
        .then((file) => {
          var gallery = JSON.parse(file || '[]');
          this.setState({
            galleryIndex: gallery.length,
            gallery: gallery
          })
        })
        .finally(() => {
          this.setState({ isLoadingGallery: false })
        })
    } else {
      const username = this.props.match.params.username;

      lookupProfile(username)
        .then((profile) => {
          this.setState({
            person: new Person(profile),
            username: username
          })
        })
        .catch((error) => {
          console.log('could not resolve profile')
        });
      
      const options = { username: username, decrypt: false };
      userSession.getFile(STATUSES_FILENAME, options)
        .then((file) => {
          var statuses = JSON.parse(file || '[]');
          this.setState({
            statusIndex: statuses.length,
            statuses: statuses
          })
        })
        .catch((error) => {
          console.log('could not fetch statuses')
        })
        .finally(() => {
          this.setState({ isLoading: false, isLoadingGallery: true })
        });
      userSession.getFile(GALLERY_FILENAME, options)
      .then((file) => {
        var gallery = JSON.parse(file || '[]');
        this.setState({
          galleryIndex: gallery.length,
          gallery: gallery
        })
      })
      .catch((error) => {
        console.log('could not fetch gallery')
      })
      .finally(() => {
        this.setState({ isLoadingGallery: false })
      })
    }
  }

  isLocal() {
    return this.props.match.params.username ? false : true;
  }
}
